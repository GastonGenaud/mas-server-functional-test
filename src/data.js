const locations = [{
  _id:
    "5f62161f0bd4a685e6f0d422"
  ,
  id: "14945859844388299845",
  __v: 0,
  account: "104179167021335829139",
  address: {
    regionCode: "AR",
    languageCode: "es-419",
    postalCode: "X5017CKU",
    administrativeArea: "Córdoba",
    locality: "Córdoba",
    addressLines: [
      "Av. Curazao 2135",
      "Santa Isabel 1ra. Sección"
    ]
  },
  createdAt: {
    date: "2020-09-16T13:41:51.881Z"
  },
  languageCode: "es-419",
  locationName: "\"PANDORA CASI TODO\"",
  locationState: {
    canUpdate: true,
    canDelete: true,
    isVerified: true,
    isPublished: true
  },
  name: "accounts/104179167021335829139/locations/14945859844388299845",
  primaryPhone: "0351 204-5689",
  updatedAt: {
    date: "2020-10-19T19:41:47.462Z"
  },
  websiteUrl: "https://pandora-casi-todo.negocio.site/?m=true",
  areaSuggestion: [
      "5f8debc23269d047c2deeb31",
      "5f8debfb3269d047c2deeb33"
  ]
}
// ,{
//   _id: {
//     oid: "5f62161f0bd4a685e6f0d423"
//   },
//   id: "3542517400904442551",
//   __v: 0,
//   account: "104179167021335829139",
//   address: {
//     regionCode: "AR",
//     languageCode: "es",
//     postalCode: "5000",
//     administrativeArea: "Córdoba",
//     locality: "Córdoba",
//     addressLines: [
//       "Av. Curazao 2135"
//     ]
//   },
//   createdAt: {
//     date: "2020-09-16T13:41:51.881Z"
//   },
//   languageCode: "es-419",
//   locationName: "\"PANDORA K-ASI TODO",
//   locationState: {
//     canUpdate: true,
//     canDelete: true,
//     isVerified: true,
//     isPublished: true
//   },
//   name: "accounts/104179167021335829139/locations/3542517400904442551",
//   updatedAt: {
//     date: "2020-10-19T19:38:40.581Z"
//   },
//   websiteUrl: "https://g.page/pandora-libreria?gm",
//   areaSuggestion: [
//     {
//       oid: "5f621baa8344971dd375a5a5"
//     },
//     {
//       oid: "5f621c0a8344971dd375a5a8"
//     },
//     {
//       oid: "5f8deb403269d047c2deeb30"
//     }
//  ]
//}
];

const rankingjobs =
[{
  "_id": {
    "oid": "5f6217578344971dd375a521"
  },
  "name": "Seo-tool-ranking",
  "zipCodes": [
    "X5017CKU"
  ],
  "cities": [],
  "keywords": [
    "librería"
  ],
  "status": "DONE",
  "typeLinkUpdate": "ONCE",
  "directories": [],
  "devices": [],
  "searchMobile": false,
  "localMobile": false,
  "searchDesktop": false,
  "localDesktop": false,
  "maps": false,
  "places": false,
  "webToolRanking": false,
  "countOfRankings": 1,
  "competitors": [],
  "serpSave": false,
  "serpOnly": false,
  "createdAt": {
    "date": "2020-09-16T13:47:03.149Z"
  },
  "updatedAt": {
    "date": "2020-10-01T18:08:22.958Z"
  }
},{
  "_id": {
    "oid": "5f621b048344971dd375a583"
  },
  "name": "Seo-tool-ranking",
  "zipCodes": [
    "5000"
  ],
  "cities": [],
  "keywords": [
    "librería"
  ],
  "status": "DONE",
  "typeLinkUpdate": "ONCE",
  "directories": [],
  "devices": [],
  "searchMobile": false,
  "localMobile": false,
  "searchDesktop": false,
  "localDesktop": false,
  "maps": false,
  "places": false,
  "webToolRanking": false,
  "countOfRankings": 1,
  "competitors": [],
  "serpSave": false,
  "serpOnly": false,
  "createdAt": {
    "date": "2020-09-16T14:02:44.670Z"
  },
  "updatedAt": {
    "date": "2020-10-01T18:08:23.109Z"
  }
},{
  "_id": {
    "oid": "5f621bb18344971dd375a5a6"
  },
  "name": "Seo-tool-ranking",
  "zipCodes": [
    "5017"
  ],
  "cities": [],
  "keywords": [
    "librería"
  ],
  "status": "DONE",
  "typeLinkUpdate": "ONCE",
  "directories": [],
  "devices": [],
  "searchMobile": false,
  "localMobile": false,
  "searchDesktop": false,
  "localDesktop": false,
  "maps": false,
  "places": false,
  "webToolRanking": false,
  "countOfRankings": 1,
  "competitors": [],
  "serpSave": false,
  "serpOnly": false,
  "createdAt": {
    "date": "2020-09-16T14:05:37.640Z"
  },
  "updatedAt": {
    "date": "2020-10-01T18:08:23.258Z"
  }
},{
  "_id": {
    "oid": "5f621c1a8344971dd375a5a9"
  },
  "name": "Seo-tool-ranking",
  "zipCodes": [
    "X5017"
  ],
  "cities": [],
  "keywords": [
    "librería"
  ],
  "status": "DONE",
  "typeLinkUpdate": "ONCE",
  "directories": [],
  "devices": [],
  "searchMobile": false,
  "localMobile": false,
  "searchDesktop": false,
  "localDesktop": false,
  "maps": false,
  "places": false,
  "webToolRanking": false,
  "countOfRankings": 1,
  "competitors": [],
  "serpSave": false,
  "serpOnly": false,
  "createdAt": {
    "date": "2020-09-16T14:07:22.105Z"
  },
  "updatedAt": {
    "date": "2020-10-01T18:08:23.410Z"
  }
}];
module.exports = {
  locations,
  rankingjobs
};