const resolvers = {
    locations: async (_, context) => {
        const {db} = await context();
        return db
            .collection("locations")
            .find()
            .toArray();
    },
    rankingjobs: async (_, context) => {
        const {db} = await context();
        return db
            .collection("rankingjobs")
            .find()
            .toArray();
    },
    location: async ({id}, context) => {
        const {db} = await context();

        return db.collection("locations").findOne({id});
    },
    rankingjob: async ({id}, context) => {
        const {db} = await context();
        return db.collection("rankingjobs").findOne({id});
    }

};

module.exports = resolvers;
